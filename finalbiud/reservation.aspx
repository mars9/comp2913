﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="reservation.aspx.cs" Inherits="MarsUI.reservation" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <link href="StyleSheet1.css" type="text/css" rel="stylesheet" />
    <div class="header">
        <h1>Mars Sports Center</h1>
        
    </div>
         <div class="navbar">
        <asp:Button ID="Login" runat="server" Text="Login" CssClass="button" OnClick="Login_Click" />
        <asp:Button ID="Home" runat="server" Text="Home" CssClass="button" OnClick="Home_Click" />
        <asp:Button ID="Facility" runat="server" Text="Facility" CssClass="button" OnClick="Facility_Click" />
        <asp:Button ID="Contact" runat="server" Text="Contact us" CssClass="button" OnClick="Contact_Click" />
        <asp:Button ID="Reservastion" runat="server" Text="Reservation" CssClass="button" OnClick="Reservation_Click" />
    </div>
        <div class="row">
            
        <div class="left">
            <h1> </h1>
        </div>
    <div class="middle">
          <asp:Label ID="lblhead" runat="server" Text="Please booking an activity for next week!" ForeColor="Green"></asp:Label>
    <br />
        <br />
        <asp:Label ID="lblfacility" runat="server" Text="facility:" ForeColor="Green"></asp:Label><asp:DropDownList ID="ddfacility" runat="server" ForeColor="Green" BackColor="#003300" OnSelectedIndexChanged="ddfacility_SelectedIndexChanged" AutoPostBack="True">
            <asp:ListItem>swimmingpool</asp:ListItem>
            <asp:ListItem>squashcourts</asp:ListItem>
            <asp:ListItem>sportshall</asp:ListItem>
        </asp:DropDownList>
        <br />
          <asp:Label ID="lblactivity" runat="server" Text="activity:" ForeColor="Green"></asp:Label><asp:DropDownList ID="ddactivity" runat="server" ForeColor="Green" BackColor="#003300">
            <asp:ListItem>team_events</asp:ListItem>
            <asp:ListItem>1_hour_sessions</asp:ListItem>
            <asp:ListItem>lane_swimming</asp:ListItem>
            <asp:ListItem>lessons</asp:ListItem>
        </asp:DropDownList>
        <br />
          <asp:Label ID="lblday" runat="server" Text="day:" ForeColor="Green"></asp:Label><asp:DropDownList ID="ddday" runat="server" ForeColor="Green" BackColor="#003300">
            <asp:ListItem>monday</asp:ListItem>
            <asp:ListItem>wednesday</asp:ListItem>
            <asp:ListItem>thursday</asp:ListItem>
            <asp:ListItem>friday</asp:ListItem>
            <asp:ListItem>saturday</asp:ListItem>
            <asp:ListItem>sunday</asp:ListItem>
        </asp:DropDownList>
        <br />
        <asp:Label ID="lbltime" runat="server" Text="time:" ForeColor="Green"></asp:Label><asp:DropDownList ID="ddtime" runat="server" ForeColor="Green" BackColor="#003300">
            <asp:ListItem>10:00to11:00</asp:ListItem>
            <asp:ListItem>11:00to12:00</asp:ListItem>
            <asp:ListItem>13:00to14:00</asp:ListItem>
            <asp:ListItem>14:00to15:00</asp:ListItem>
            <asp:ListItem>15:00to16:00</asp:ListItem>
            <asp:ListItem>16:00to17:00</asp:ListItem>
            <asp:ListItem>17:00to18:00</asp:ListItem>
            <asp:ListItem>18:00to19:00</asp:ListItem>
            <asp:ListItem>19:00to20:00</asp:ListItem>
        </asp:DropDownList>
        <br />
        <asp:Label ID="lblname" runat="server" Text="name:" ForeColor="Green"></asp:Label><asp:TextBox ID="txtname" runat="server"></asp:TextBox>
        <br />
        <asp:Label ID="lbltel" runat="server" Text="tel:" ForeColor="Green"></asp:Label><asp:TextBox ID="txttel" runat="server"></asp:TextBox>
        <br />
        <asp:Label ID="lblemail" runat="server" Text="email:" ForeColor="Green"></asp:Label><asp:TextBox ID="txtemail" runat="server"></asp:TextBox>
        <br />
        <asp:Label ID="lblfee" runat="server" Text="fee:" ForeColor="Green"></asp:Label><asp:Label ID="lblpay" runat="server" Text="20" ForeColor="Green"></asp:Label><asp:Label ID="lblmoney" runat="server" Text="£" ForeColor="Green"></asp:Label>
        <br />
        <asp:Label ID="lblselect" runat="server" Text="Select payment method:" ForeColor="Green"></asp:Label>
    <br />
    <asp:RadioButtonList ID="RadioButtonList2" runat="server" OnSelectedIndexChanged="RadioButtonList2_SelectedIndexChanged" ForeColor="Green">
        <asp:ListItem>Card</asp:ListItem>
        <asp:ListItem>Cash</asp:ListItem>
        </asp:RadioButtonList>
        <asp:Label ID="lblcard" runat="server" Text="If you pay by card, please enter CardNumber:" ForeColor="Green"></asp:Label><asp:TextBox ID="txtcardNumber" runat="server"></asp:TextBox>
       <br />
    
        <asp:Button ID="btnconfirm" runat="server" Text="confirm" CssClass="buttonmid" OnClick="btnconfirm_Click" />
        </div>

        </div>
        <div class="footer">
        <h2>© Copyright - Mars 2020</h2>
    </div>
    </form>
</body>
</html>
