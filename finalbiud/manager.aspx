﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="manager.aspx.cs" Inherits="MarsUI.manager" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <link href="StyleSheet1.css" type="text/css" rel="stylesheet" />
    <div class="header">
        <h1>Mars Sports Center</h1>
        
    </div>
         <div class="navbar">
        <asp:Button ID="Login" runat="server" Text="Login" CssClass="button" OnClick="Login_Click" />
        <asp:Button ID="Home" runat="server" Text="Home" CssClass="button" OnClick="Home_Click" />
        <asp:Button ID="Facility" runat="server" Text="Facility" CssClass="button" OnClick="Facility_Click" />
        <asp:Button ID="Contact" runat="server" Text="Contact us" CssClass="button" OnClick="Contact_Click" />
        <asp:Button ID="Reservastion" runat="server" Text="Reservation" CssClass="button" OnClick="Reservation_Click" />
    </div>
        <div class="row">
            
        <div class="left">
            <h1> </h1>
        </div>
    <div class="middle">
        <asp:Label ID="Label1" runat="server" Text="overall income:" ForeColor="Green"></asp:Label><asp:Label ID="lbloverall" runat="server" Text="" ForeColor="Green"></asp:Label>
        <br />
        <asp:Label ID="Label2" runat="server" Text="Swimmingpool usage:" ForeColor="Green"></asp:Label><asp:Label ID="Lblswim" runat="server" Text="" ForeColor="Green"></asp:Label>
         <br />
        <asp:Label ID="Label3" runat="server" Text="Squashcourts usage:" ForeColor="Green"></asp:Label><asp:Label ID="lblsquash" runat="server" Text="" ForeColor="Green"></asp:Label>
         <br />
         <asp:Label ID="Label4" runat="server" Text="Sportshall usage:" ForeColor="Green"></asp:Label><asp:Label ID="lblhall" runat="server" Text="" ForeColor="Green"></asp:Label>
         <br />
        <asp:Label ID="Label5" runat="server" Text="Team events usage:" ForeColor="Green"></asp:Label><asp:Label ID="lblteam" runat="server" Text="" ForeColor="Green"></asp:Label>
         <br />
        <asp:Label ID="Label6" runat="server" Text="1 hour sessions usage:" ForeColor="Green"></asp:Label><asp:Label ID="lbl1hopur" runat="server" Text="" ForeColor="Green"></asp:Label>
         <br />
        <asp:Label ID="Label7" runat="server" Text="lane swimming usage:" ForeColor="Green"></asp:Label><asp:Label ID="lbllane" runat="server" Text="" ForeColor="Green"></asp:Label>
         <br />
        <asp:Label ID="Label8" runat="server" Text="lessons usage:" ForeColor="Green"></asp:Label><asp:Label ID="lbllesson" runat="server" Text="" ForeColor="Green"></asp:Label>
         <br />
          <asp:Button ID="btnfinish" runat="server" Text="finish week" CssClass="buttonmid" OnClick="btnfinish_Click" />
    <br />
        </div>
        </div>
            <div class="footer">
        <h2>© Copyright - Mars 2020</h2>
    </div>
    </form>
</body>
</html>
