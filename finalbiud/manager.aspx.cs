﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MarsUI
{
    public partial class manager : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager
     .ConnectionStrings["connectionString"].ConnectionString);
            conn.Open();
            SqlCommand command;
            //count the suage of every facility and activity
            String countswim = "select count(*) from reservation where facility='swimmingpool'";
            command = new SqlCommand(countswim,conn);
            command.ExecuteScalar();
            int swim = Convert.ToInt32(command.ExecuteScalar());
            Lblswim.Text = swim.ToString();
            String countsquash = "select count(*) from reservation where facility='squashcourts'";
            command = new SqlCommand(countsquash, conn);
            command.ExecuteScalar();
            int squash = Convert.ToInt32(command.ExecuteScalar());
            lblsquash.Text = squash.ToString();
            String counthall = "select count(*) from reservation where facility='sportshall'";
            command = new SqlCommand(counthall, conn);
            command.ExecuteScalar();
            int hall = Convert.ToInt32(command.ExecuteScalar());
            lblhall.Text = hall.ToString();
            String countteam= "select count(*) from reservation where activity='team_events'";
            command = new SqlCommand(countteam, conn);
            command.ExecuteScalar();
            int team = Convert.ToInt32(command.ExecuteScalar());
            lblteam.Text= team.ToString();
            String count1hour = "select count(*) from reservation where activity='1_hour_sessions'";
            command = new SqlCommand(count1hour, conn);
            command.ExecuteScalar();
            int hour = Convert.ToInt32(command.ExecuteScalar());
            lbl1hopur.Text = hour.ToString();
            String countlesson = "select count(*) from reservation where activity='lessons'";
            command = new SqlCommand(countlesson, conn);
            command.ExecuteScalar();
            int lesson = Convert.ToInt32(command.ExecuteScalar());
            lbllesson.Text = lesson.ToString();
            String countlane = "select count(*) from reservation where activity='lane_swimming'";
            command = new SqlCommand(countlane, conn);
            command.ExecuteScalar();
            int lane = Convert.ToInt32(command.ExecuteScalar());
            lbllane.Text = lane.ToString();
            //count total income
            int total = swim * 20 + squash * 20 + hall * 30;
            lbloverall.Text = total.ToString() + "£";
            conn.Close();
        }

        protected void btnfinish_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager
     .ConnectionStrings["connectionString"].ConnectionString);
            conn.Open();
            SqlCommand command;
            //detete table data
            String finish = "turncate table [reservation]";
            command = new SqlCommand(finish, conn);
            command.ExecuteScalar();

        }

        protected void Contact_Click(object sender, EventArgs e)
        {

        }

        protected void Reservation_Click(object sender, EventArgs e)
        {

        }

        protected void Login_Click(object sender, EventArgs e)
        {

        }

        protected void Home_Click(object sender, EventArgs e)
        {

        }

        protected void Facility_Click(object sender, EventArgs e)
        {

        }
    }
}