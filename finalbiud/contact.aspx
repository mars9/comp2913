﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="contact.aspx.cs" Inherits="MarsUI.contact" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <link href="StyleSheet1.css" type="text/css" rel="stylesheet" />
    <div class="header">
        <h1>Mars Sports Center</h1>
        
    </div>
         <div class="navbar">
        <asp:Button ID="Login" runat="server" Text="Login" CssClass="button" OnClick="Login_Click" />
        <asp:Button ID="Home" runat="server" Text="Home" CssClass="button" OnClick="Home_Click" />
        <asp:Button ID="Facility" runat="server" Text="Facility" CssClass="button" OnClick="Facility_Click" />
        <asp:Button ID="Contact" runat="server" Text="Contact us" CssClass="button" OnClick="Contact_Click" />
        <asp:Button ID="Reservastion" runat="server" Text="Reservation" CssClass="button" OnClick="Reservation_Click" />
    </div>
        <div class="row">
            
        <div class="left">
            <h1> </h1>
        </div>
    <div class="middle">
          <asp:Label ID="lblhead" runat="server" Text="Our information" ForeColor="Green"></asp:Label>
    <br />
         <asp:Label ID="Label1" runat="server" Text="Sports events/booking team" ForeColor="Green"></asp:Label>
    <br />
         <asp:Label ID="Label2" runat="server" Text="Sports activity office" ForeColor="Green"></asp:Label>
    <br />
         <asp:Label ID="Label3" runat="server" Text="Old Downtown" ForeColor="Green"></asp:Label>
    <br />
         <asp:Label ID="Label4" runat="server" Text="Night City" ForeColor="Green"></asp:Label>
    <br />
         <asp:Label ID="Label5" runat="server" Text="NC26YH" ForeColor="Green"></asp:Label>
    <br />
         <asp:Label ID="Label6" runat="server" Text="Tel: 0700123456" ForeColor="Green"></asp:Label>
    <br />
         <asp:Label ID="Label7" runat="server" Text="Email: sc19tw2@leeds.ac.uk" ForeColor="Green"></asp:Label>
    <br />
        <br />
        <br />
        <br />
        <br />
         <asp:Label ID="Label8" runat="server" Text="Membership service team" ForeColor="Green"></asp:Label>
         <br />
        <asp:Label ID="Label9" runat="server" Text="Membership office" ForeColor="Green"></asp:Label>
         <br />
        <asp:Label ID="Label10" runat="server" Text="Charter Hill" ForeColor="Green"></asp:Label>
         <br />
        <asp:Label ID="Label11" runat="server" Text="Night City" ForeColor="Green"></asp:Label>
         <br />
        <asp:Label ID="Label12" runat="server" Text="NC45WE" ForeColor="Green"></asp:Label>
         <br />
        <asp:Label ID="Label13" runat="server" Text="Tel: 07561536450" ForeColor="Green"></asp:Label>
         <br />
        <asp:Label ID="Label14" runat="server" Text="Email: sc19yl2@leeds.ac.uk" ForeColor="Green"></asp:Label>
         <br />
        <br />
        <br />
        <br />
        <asp:Label ID="Label15" runat="server" Text="The Mars Reception team" ForeColor="Green"></asp:Label>
         <br />
        <asp:Label ID="Label16" runat="server" Text="Reception office" ForeColor="Green"></asp:Label>
         <br />
        <asp:Label ID="Label17" runat="server" Text="Charter Hill" ForeColor="Green"></asp:Label>
         <br />
        <asp:Label ID="Label18" runat="server" Text="Night City" ForeColor="Green"></asp:Label>
         <br />
        <asp:Label ID="Label19" runat="server" Text="NC45WE" ForeColor="Green"></asp:Label>
         <br />
        <asp:Label ID="Label20" runat="server" Text="Tel: 07596459547" ForeColor="Green"></asp:Label>
         <br />
        <asp:Label ID="Label21" runat="server" Text="Email: ml18a5l@leeds.ac.uk" ForeColor="Green"></asp:Label>
         <br />
        </div>

        </div>
        <div class="footer">
        <h2>© Copyright - Mars 2020</h2>
    </div>
    </form>
</body>
</html>
