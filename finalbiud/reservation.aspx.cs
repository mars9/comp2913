﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MarsUI
{

    public partial class reservation : System.Web.UI.Page
    {
        public string username;
        protected void Page_Load(object sender, EventArgs e)
        {
            UnobtrusiveValidationMode = UnobtrusiveValidationMode.None;
            //get user
            username = Session["username"].ToString();
            if (username == "")
            {
                Response.Write("<script>alert('You must log in to do reservation!')</script>");
            }
           
        }
        protected void Login_Click(object sender, EventArgs e)
        {

        }

        protected void Home_Click(object sender, EventArgs e)
        {

        }

        protected void Facility_Click(object sender, EventArgs e)
        {

        }

        protected void Contact_Click(object sender, EventArgs e)
        {

        }
        protected void Reservation_Click(object sender, EventArgs e)
        {

        }

        protected void ddfacility_SelectedIndexChanged(object sender, EventArgs e)
        {
            String facility = ddfacility.Text.ToString();
            //change price
            if (facility == "sportshall")
            {
                lblpay.Text = "30";

            }
            else
            {
                lblpay.Text = "20";
            }
        }

        protected void btnconfirm_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager
     .ConnectionStrings["connectionString"].ConnectionString);
            conn.Open();
            SqlCommand command;
            String facility = ddfacility.Text.ToString();
            String activity = ddactivity.Text.ToString();
            String time = ddtime.Text.ToString();
            String day = ddday.Text.ToString();
            String name = txtname.Text.ToString();
            String tel = txttel.Text.ToString();
            String email = txtemail.Text.ToString();
            int fee = Convert.ToInt32(lblpay.Text);
            //check if the activity is avaliable
            String atvcheck = "select count(*) from [activity] where fid=(select fid from facility where fname='" + facility + "') and activityname='" + activity + "' and activitytime='" + time + "' and day='" + day + "'";
            command = new SqlCommand(atvcheck,conn);
            command.ExecuteScalar();
            int check= Convert.ToInt32(command.ExecuteScalar());
            if (check == 0)
            {
                Response.Write("<script>alert('there is no avaliable activity, please check our time table!')</script>");
            }
            else {
                //count usage of every facility
                String swimcount = "select count(*) from [reservation] where aid=(select aid from activity where fid='1' and activityname='" + activity + "' and activitytime='" + time + "' and day='" + day + "')";
                command = new SqlCommand(swimcount, conn);
                command.ExecuteScalar();
                int swim= Convert.ToInt32(command.ExecuteScalar());
                String squashcount= "select count(*) from [reservation] where aid=(select aid from activity where fid='3' and activityname='" + activity + "' and activitytime='" + time + "' and day='" + day + "')";
                command = new SqlCommand(squashcount, conn);
                command.ExecuteScalar();
                int squash = Convert.ToInt32(command.ExecuteScalar());
                String hallcount= "select count(*) from [reservation] where aid=(select aid from activity where fid='4' and activityname='" + activity + "' and activitytime='" + time + "' and day='" + day + "')";
                command = new SqlCommand(hallcount, conn);
                command.ExecuteScalar();
                int hall = Convert.ToInt32(command.ExecuteScalar());
                if (swim >= 32 || squash >= 4 || hall >= 20)
                {
                    Response.Write("<script>alert('your reserved facility is out of capacity')</script>");
                }
                else
                {
                    try
                    {
                        //
                        string chkvalue = "select amount from orders where id=1";
                        command = new SqlCommand(chkvalue, conn);
                        command.ExecuteScalar();
                        int value = Convert.ToInt32(command.ExecuteScalar());
                        value = value + 1;
                        //update order
                        string udvalue = "update orders set amount ='" + value + "' where id=1";
                        command = new SqlCommand(udvalue, conn);
                        command.ExecuteScalar();
                        String rid = "Reservation" + Convert.ToString(value);
                        //get activity id
                        String getaid = "select aid from activity where fid=(select fid from facility where fname='" + facility + "') and activityname='" + activity + "' and activitytime='" + time + "' and day='" + day + "'";
                        command = new SqlCommand(getaid, conn);
                        command.ExecuteScalar();
                        String aid = Convert.ToString(command.ExecuteScalar());
                        string date = Convert.ToString(System.DateTime.Now.ToString("d"));
                        string rtime = Convert.ToString(System.DateTime.Now.ToString("T"));
                        //uptate reservation table
                        String reserve = "insert into reservation values('" + rid + "','" + aid + "','" + date + "','" + name + "','" + tel + "','" + email + "','"+facility+"','"+activity+"')";
                        command = new SqlCommand(reserve, conn);
                        command.ExecuteScalar();
                        //add reciept
                        string udreceipt = "insert into Receipt values('" + rid + "','"+username+"','" + name + "','" + date + "','" + rtime + "','reservation','" + fee + "','paid')";
                        command = new SqlCommand(udreceipt, conn);
                        command.ExecuteScalar();
                        Session["reciept"] = rid;
                        Response.Redirect("Receipt.aspx");
                    }
                    catch
                    {
                        Response.Write("<script>alert('reservation failed')</script>");
                    }
                }
            }
            conn.Close();




        }

        protected void RadioButtonList2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}